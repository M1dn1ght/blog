---
title: Blogging vs. journaling
date: "2019-12-16T00:00:00.002Z"
description: "An excerpt from my digital journal that contains thoughts on drugs and the differences between journaling and blogging."
---
Last night I did some journaling after my blogging, and found that they are quite different to me. Here is an excerpt from my digital journal, edited a bit for pride's sake since I sound like such a drug addict:

 >fucked around with the blog. not sure if i like it, seems needlessly complex. but thats computer science in a nutshell. has to be robust, so it ends up complex. itd prob be better if it had a background. will do that on professional one tomorrow.

>guess i gotta go to sleep. if i had [drugs] id do some. its fun to stay awake. [some talk about the various stimulants and my feelings on them and the stigmas around them]

>why is journaling so much easier than blogging? are they not the same? these are my thoughts here. i guess the blog im trying to filter my thoughts? this is raw unfiltered.

>now i am thinking of drugs. i could try the naltrexone, i believe monday was the cutoff for 6 days of kratom free. but should maybe wait til my body resets a lil with sleep cause of the [drugs]. even though it prob doesnt fuck with opiate receptors.

>fuck man. drugs would be nice. i wonder if its possible? contentment as a drug addict? no, probably not. then why do i want that life? because anything is better than nothing right.

>cloning ~250 mb of dev shit i prob dont need on phone, thats the only problem with sticking blog in here. have no space on it. oh no jk, the gitignore blocks most of the dev shit right? thats good.

>seamless transition between phone and pc. you cant even tell i changed. i wonder if i should put a dash line. tomorrow getting some domain names and web hosting.

>really want drugs now shouldnt have mentioned them. gonna take [my nightly perscriped downers]. and honestly might add a hit of caffeine lmao. not to stay up on purpose just cause idk it feels right. fuck it man.

>every shooting star, reminds me when, hollywood lost the brightest.

>wow the line numbers look so small! 16! was at like 3k in both phone and pc i think. how can i have so many thoughts? easy, they repeat. imagine if they were all unique! id be a genius. should try to have more unique thoughts. thats what doing things is about, stimulation. its easier to get from drugs. drugs. 2020. job. money. bad at life. bad at love. bonbi. friends. weed. thoughts faster than i can type them. last year i wished i was dumber. do you still wish that? do you feel consciousness is a gift or a curse? a curse.

>excited for tomorrow though, think i will wake up happy. or at least not wake up fuckin crazy depressed. then again drug cravings usually persist through the night. may try the naltrexone. these arent interesting, are they? my mundane every day thoughts? thats why journaling is easier, it has no substance. its just a narration. stream of consciousness.

>im thinking i might post this on my blog. just to see how it feels. or maybe edit it down. just do a post journaling vs. blogging. sounds hard.

>i dont want to go to bed. i want to take [drugs] and fuck around on pc. drugs.
got back on pc. thinkin i might do some div readings [tarot reads for the divination general thread on 4chan's /x/] or online shopping.

Posting this made me realize my journal is not markdown friendly, the line breaks don't actually line break. Damn and now that I added line breaks the italics is messed up. Maybe will try as a block quote.

I hope this piece of my consciousness was interesting to you, I enjoyed posting it.
