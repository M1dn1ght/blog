---
title: Tarot blog - how to get gf?
date: "2018-03-12T00:00:00.001Z"
description: A short post describing a short tarot read I did.
---

A few weeks ago I did a 3 card question reading. Was feeling quite lonely at the time, it was a few days before Valentine's day when I initially queried my Inner Guide. I asked the Universe "What can I do to be more attractive to young women that I would like to have sex with?" This is the spread I got:

[pic]

My initial reaction to the spread was quite negative. The presence of the 10 of Swords and 2 of the major arcana seemed to suggest it would be hard work to be more a more attractive young man.

Upon reflection, here is what I believed the Universe was trying to tell me: *In the past a lot of my problems with women came from insecurity and martyrdom. In the present, I seek to join forces inside myself and create a balance. I need to balance self love with giving love to others! Romance all the way to rebirth.*

So to answer the original question: *Don't be a victim. Find the balance between self love and love for women. Be reborn into a new romance and/or a more attractive person.*

---

## Update December 16, 2019
This is one of the few old posts that I can bear to read so far. I was cringey but I was happy. It was the happiest I've been honestly, when the SSRI's first started working and the positive self talk made me feel so good and the weed was still fresh and exciting.

I still do a good amount of tarot. I'm glad it found its way into my life, has provided a lot of insight. Maybe that will be mostly what this blog is about, I'll collect some tarot reads.
