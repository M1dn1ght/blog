---
title: My return to blogging, 2020 starting early
date: "2019-12-16T00:00:00.001Z"
description: "A rambling post. The original intent was to describe my summer. Instead I wrote nothing. Just kidding, kinda."
---
*Greetings internet. Thought I'd bust my old blog out since I'm back on the SSRI's and almost feeling good again. I'm here to tell you about my life and you can compare it to your own. They say the internet is forever, but my blog is not up there anymore and nobody ever read it so...? It's more like inexistence than permanence. Or perhaps now that I am putting it back up, it will be there forever. I only register the domain for a year each time. But the blog itself can continue to exist as data. I still don't think the internet is forever, I think small things on the internet are the opposite, they are fleeting.*

*I had the worst summer I've ever had. Well, at least one of the worst. I spent it in bed. I am mentally ill. I had to quit smoking weed and I miss it so fucking much.*

*Getting angry now. Can't fucking write good, brain too fogged from the sleeplessness.*

### BUT WHO'S GONNA READ IT ANYWAYS!

---

***Every shooting star, reminds me when Hollywood, lost the brightest... that shine, that spark, the sun set too soon that night***

---

*My old posts are cringey. My new posts will probably be cringey as well. Don't know if I should even post them (the old ones) here, I wouldn't write them today.*

*The title of this post is way off. Haven't talked about returning to blogging, haven't talked about starting 2020 in December. Don't know what to say about blogging. Looking forward to it I guess. This is the same garbage I write in my journals every day, except now the whole world can read it. I guess a blog should provide something? Create value? Entertainment, information. But entertainment is subjective. You could be an odd one and find irrelevant rambling entertaining. Or you could be a friend or love or enemy of mine and enjoy the insights into my head. Blogging is also kind of out of fashion isn't it? All the bloggers are "journalists" now. Because every site is a news site.*

*And yes I am starting 2020 early. I need a new year. My new year's resolution is stay out of bed. I'm tired of lying in bed. Ha, now I'm on someone else's blog reading about lay vs. lie cause I wanted to write laying in bed, but that I found out recently that is incorrect. Except it's probably not a blog, just a website.*

> Hello and welcome to my website. My name is Jakub Marian, and I am a Czech linguist, data scientist, graphic designer, and musician.

*So just a website, as we predicted.*
