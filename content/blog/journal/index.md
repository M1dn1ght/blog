---
title: Stream of my depressed consciousness
date: "2020-01-05T00:00:00.001Z"
description: "Some more excerpts from my digital journal."
---
Bored and caffeinated so going to post some more of my journal for the internet. I left it mostly unedited, neurotic line breaks and irrelevent ramblings intact. Selected 3 to 4 days, linked by thoughts on 3 to 4 dog walks. Starts out super depressed but perks up a few days in I think. Enjoy.

### 2019/12/28
>walking dog in the snow made me think of drugs. nighttime snow, reminds me of something. maybe one time with [friend]. but i think i was sober then. maybe some time at rutgers. but also makes me think of [hometown].

>gonna take all meds. that should help right? naltrexone in there, that should help. downers help, take the edge off.
fucking drugs man. how can i be so far from the one thing i care about?

>fuckin drugs. drugs. drugs. drugs. drugs. drugs. drugs.

>order some phenibut.

>hate my fucking hair.

>kinda freaking out. feel like shit from all the candy. hate my fucking life, hate my self. im sorry. hate hate hate. i fucking failed. all ive ever done is fail. the fucking fizzy water can is making fucking noises.

>lets go back over it. another round of "its good that i feel like shit, ill appreciate it more when i feel good." honestly thats true. ive been loving the few good moods.

>i just wish shit was different man. so make it different. i just wish shit was fuckin different.

>fuck.

>it comes back to fucking weed. i miss it so fucking much. never again am i going on probation or anything similar. almost done. pretty much january.

>i feel bad. feel suicidal. how can i still feel so fucking bad???????? its been fucking months. years. why cant i get any fucking happier?????????? i did. i was happy. so why cant i do that now. i was astounded to learn i could just say "i am strong" and be it. i cant do it now because i stopped believing it. stopped believing you could just say things and make them true. evidence. my therapist says it works better with evidence. i am strong because i havent killed myself yet. i am strong because i havent smoked weed.

>i am strong. i can force myself to be happy. i am strong enough to be happy. happy. happy. happy. happy.

>so unhappy. was going to say the meds are making it worse but now i kinda feel they are helping.

>just dont like my life. havent for a while.

>[blank space for formatting] 2020. 2020 is the year. its a new decade. new decade new me. can leave all this shit in the past.

>yeah the downers are helping. feel bit more relaxed. wish i could smoke weed. ik ik ik i shouldnt focus on it but its impossible not to. soon man. soon. few more months. few more months is nothing. 2/3 of the way through. all thats left is half the time of what we've been through so far. not that long at all. and at least it cant get any worse right? lol hope im not jinxing it. but seriously theres only one way to go and thats up. any step in any direction is a step in the right direction.

>i am strong. strong because i still believe. still fucking believe man, believe with all my heart that being happy is possible. maybe not all my heart but most of it. honestly just gotta survive a few more months.

>wishing for rewind surprisingly since we were just talking about the future [i often debate wishing for rewind vs. fast forward in this journal]. want to go back to rutgers. had friends and fun. can get those things back?

---

>woke up and cant go back to sleep. fantasizing anout a young girl. had dream or fantasies about pokemon and other stuff. too lazy to write it.
gonna start a new day [in terms of jouranl entries] and hopefully get out of bed.

### 2019/12/29
>fuck. another day of hating life?

>another day of laying in fucking bed???????????

>i just cant do it. yes i can.

>i can do things. i can get out of bed because i have before.

>out of bed. washing sheets. gonna shower.

>hard. hard.

>worked a bit on prof blog. nice job. good job. nice job.

>now what? shower? poe [path of exile]? mario maker? [some stuff about mario maker]

>love my pc. grateful for my pc. thank you.

>lost.

---

>miss weed. used to get high and play poe.

>lost.

>what am i doin man? fuckin nothing. so sick of my life. gotta do something different today. gonna shower and go to the mall or something.

---

>dads showering. jk i think he finished. will wait 5 mins or so for hot water.

>fucking depressed dude. the caffeine is not helping. maybe i should take my psychiatric medications. they are supposed to help my depression.

>took all meds. i wish they would actually help. i wish i was taking something better. amphetamines, methamphetamines, benzodiazepines. upper or downer would be nice. thats important to note right? [mental health professionals want me to catalog the drug cravings and ponder what they mean] that means you just wanna be high. but i already knew that?

>im just so fucking unhappy. its like i am stuck in a fuckin rut at the roadside. stuck in the mud, the snow. and ive been here for fuckin months, years. car wont move. tried fucking everything. it moved a bit, earlier. but not anymore.

>and of course it comes back to weed. id be happy if i had weed. but thats not true right? even when i had weed i wasnt happy. never been fucking happy.

>dude. i am really depressed. it fucking blows. i just feel so fucking bad, it feels like life is garbage. it feels like ill never do anything. thats not true. dude idk wtf im supposed to do. i just feel so fucking bad man. why?????????????????????????????????????????????????????????????????????????????? why am i so fucking broken? im supposed to be happy. or at least baseline. im supposed to be at baseline. but instead im at fucking low point. physically i feel ok. emotionally i feel surprisingly ok. but mentally i feel fucking lost and broken and weak. i hate my brain. we hate each other. it sucks. and this makes it worse right? not supposed to say that. supposed to love my self.

>dude.

>coloring helped a bit. shower still seems like the hardest fucking thing in the world. disappointed in the psych meds. shouldnt they make things easier? adderall would. weed would distract me. what do these do?

---

>shower was fucking hard. didnt shave or condition or wash my face. but at least i fucking did it right? and now go to the mall and sit by my fucking self. buy nothing except a coffee. mall opens at 11.

>so fucking depressed dude. the meds do nothing. fucking fuck.

>dude. i just want to smoke weed. what can i get high off of? fucking nothing. out of options.

>what do i do???????????????????????????????????????????????????????????????????????????????? please tell me. [tarot read to see what to do]

#### *6 of pentacles - 2 of cups - 5 of swords*

>6 of pents fuckin again. get a job, its screaming it at me. right? lack of resources.

>into 2 of cups. ask for help?

>into 5 of swords. stop focusing on self? or maybe the right side is about relationships.

>thank you universe. i kind of already knew all that. get a job. get a job. get a job. get a job. get a job. get a job. get a job. get a job. get a job. get a job. get a job. get a job. get a job. get a job. get a job. get a job. get a job. get a job. get a job. get a job. get a job. get a job. get a job. get a job. get a job. get a job.

>fucking hell man. i can barely take a fucking shower. need 400 mg of caffeine to take a fucking shower. how much to apply for jobs then??? a million milligrams???????? makes sense to me.

>poor diet, lack of exercise. these are things i can control. i just have convinced myself that they are too hard. that feels out of my control. this concept that everything is so fucking hard seems forced on me. thats my mental illness. right??????

>thinkin ill just go to the starbucks by me. then again its already 10:36 [the mall didn't open until 11].

>i just feel really depressed. i am disappointed in the psych meds. i am writing the same things over and over and over. does it help? no.

>i am grateful for.................

>i accomplished taking a shower. i got out of bed. great fucking accomplishments. they are great to me. i did them. i colored. i worked on blog a bit. i ran some maps in path of exile. i am doing things.

---

>at the starbucks. now what??????????????????

---

>dont think i journaled at all on pc. on the [comfy chair in living room] now. still feel pretty bad honestly. vidya and voice chat with the bros distracted me. but now im off and im back to feeling fucking bad man. just feel like life sucks.

>poor diet. lack of exercise. try fixing these things then you can complain. no fuckin way im getting another fuckin canker sore dude. right at the edge of my lip. maybe is just wound from picking skin off. hopefully.

>guess im getting back on pc for more poe, thats all that seems appealing. cant get into [streamer] vod. out of starcraft [competitive vods].

>just more of the same bullshit. just killing time until i get weed back. almost there.

---

>wanted to write something. the walk tonight with dog, it wasnt as dark and quiet in the snow. went quick way, down [main road]. so wasnt thinking about drugs in the same way as last night or some other recent evening. and yet i was still thinking of drugs in another way. was thinking about [dog] and what he sees/smells/"thinks." and he smells the nasty benzedrex and knows ill be doin weird stuff. he used to smell the weed, the vodka. was thinking how he knows drugs.

>anyways, day is winding down now. 8:37 pm. just put pc and switch to sleep. played a lot of vidya today. poe and monster hunter.

>think i might turn switch back on lol. and then might as well go afk in poe to sell stuff.

>feel kinda bad. felt bad earlier and took extra beta blocker. felt uncomfortable. still feel uncomfortable.

>i want to fit more snugly into my life. i feel i kick and scream, drag my feet trying to live my life. parts of me do. some parts are overactive. some parts have taken on more than they can handle. i can help them.

>do they want to fit more snugly into life? yes. so lets do that.

>i have to put my sheets on my bed. i dont want to. considered taking caffeine just to do it lmao. i am broken.

>life is just so fucking hard man. why is everything so fucking hard for me. fuck dude. im not supposed to say that right? i dont fucking care. shit is fucking hard idc if thats the wrong attitude thats just how it fucking feels.

>fuck. i seriously am broken, i dont work right. and i havent found anything that fixes me. hopefully i will. i hope i will. i dont know it but i hope it. and thats what hope is about right? you dont know, you never know. all you can do is hope. no, you can do more than hope right? you can put the effort in, direct energy towards the things you want. at least theoretically. for normal people. and for me. i can put effort in. i can put forth effort because ive done it before. i can do things because ive done things.

>i did it [put sheets on bed]. i dont feel any fucking better. if anything i feel fucking worse. cause its not perfect. and i didnt finish. i didnt put pillowcase on spare pillow.

>i fucking did it though. i can fucking do things. i can do things. i can do things. tomorrow i am hanging shit up. tomorrow i am hanging shit up. i can do things. i can do things. i can do things. i can do things. [midnight]. [midnight] can do things. names. how important are names? names are weird. so important and yet so arbitrary.

>[some stuff about my name]

>if i can do things that means things can be different right? i can make things different. by doing things. i dont believe it. its just never changed, never been different. thats not true. it was different in 2018. i loved myself. i loved my self. i lost that. self love is a journey i guess.

>i love my self. i love me. flaws are good. flaws make me real, make me human. i am only human. flaws are good.

>i love my self. me, this voice. all voices, all parts. all feelings. all fears. i love them. i love the parts that experience anxiety. i dont love the anxiety.

>my mental illness is not me. its an illness. im doing my best with what i have. im doing my best. im doing my best. im doing my best. it feels like my best isnt enough. it feels like im not doing my best, like my best should be more.

>no. its hard and im doing it anyway.

>fuck.

>i just wish it wasnt so fucking hard man. i wish shit would fucking change even a little bit from all this fucking willing i do, all this fucking mental energy put into asking why.

>i wish it was easier. i wish life was easier. my life is fucking easy already. my life is easy. but its hard???????????

>holy fuck dude. i feel bad. need fucking stimulants. im fucking broken, i dont run right. i dont fucking care that its the wrong attitude, false belief whatever. its just how i fucking feel.

>supposed to imagine what it would be like if i ran correctly. if i was successful, confident. imagine that and step into it.

>helped i guess? made me want to brush my teeth. didnt. so it didnt work right?

>brain feels sluggish. was following a train of thought about parts of me and "what if the parts holding me back didnt exist?" but i couldnt discern which parts didnt want to brush teeth, and then i kind of realized me myself didnt want to brush my teeth. but yeah brain feels bad. it was muddled, didnt make sense. could hear [family] talking. usual pang of jealousy [often feel I don't connect as easily with my family as they do each other]. then i realized i dont even like when i talk to mom. so why would i be jealous?

>it will take time. to become more positive.

>i can do things. i can do things. i can do things.

>going to install phone update. im hoping it doesnt make my phone worse, i put it off this long because it says "android 9.0" which seems like a serious update. going to move to physical journal, maybe notebook maybe [other journal]. probably notebook. or maybe do some coloring. or poe or blogging. prob gonna go for some caffeine honestly. im happiest stimulated. wont be happy tomorrow if i stay up all night. then again i can prob sleep on it with this tolerance. ok ill see you soon.

>the update didnt pop up.

>guess i should put on [streamer I usually watch before bed]. dont really want to. took evening downers. would rather go the other direction. adderall or meth. would eat a bit and sniff a bit. rail some stimulants. would be nice.

>thats all ive ever fucking wanted man. just drugs. except when i was a kid. even as a kid i wanted to be more than human.

>drugs. drugs. get a job. fast forward to end of probation. latter is the best option, but its not possible. other mentioned option would help, job would fill up time.

>i can do it because ive done it before. thats logical.

### 2019/12/30
[coming soon]
