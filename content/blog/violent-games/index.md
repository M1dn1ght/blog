---
title: Violent video games and violence
date: "2018-02-24T00:00:00.001Z"
description: My opinion on whether or not violent video games cause violence.
---

I was born in 1995. I grew up playing video games. A lot of them violent. Some of my fondest memories are playing Call of Duty Modern Warfare 2 and Black Ops 1 on Xbox live. This post will mainly be about my opinions on the theory that "violent video games cause violence."

I truly believe that violent video games do not cause violence. Perhaps I am wrong and they do, but they certainly did not in my case. I understand the various arguments; they romanticize violence, they reward kids for practicing violence, they glorify violence. But in all honesty, kids are smart enough to realize that video game violence is not real.

When I was quickscoping noobs in MW2, I understood that it was completely separate from reality. It wasn't even really about the "violence," that's just the simplest way to design a competitive scenario. It's more about the actual game. The competition. Some of the most popular video games had no violence at all, but were still iconic video games because of their competitive nature. Look at Rocket League, it is a very popular e-sports type game without any violence. Or any classic game like Pong or Super Mario Brothers.

Another fact is that violence makes for interesting and fun gameplay. You don't have an action movie without violence; do these same people say that action movies make people violent? I suppose you wouldn't take your kid to an action movie, but that's why games have the ESRB ratings

---

## Update December 16, 2019
I said "quickscoping noobs" ironically, idk why it looks so sincere. And I also feel like Super Mario Brothers does have violence.

God I fucking hate these old posts. That's probably not a healthy attitude, but it's true. Every fucking year I cringe at the last year. Why? Why can't I just be me and like it?

Gonna post more tarot ones, that should be ok.
